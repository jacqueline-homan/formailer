{-# LANGUAGE FlexibleContexts, FlexibleInstances,
    MultiParamTypeClasses, ScopedTypeVariables,
    TypeFamilies, TypeSynonymInstances,
    QuasiQuotes, OverloadedStrings #-}

module APIv1 where

import Control.Monad.IO.Class (liftIO)
import Data.Text (Text, pack)
import Happstack.Server
import Formail.Configuration (FormailConfiguration(..))
import Formail.Database
import Data.Time.Clock (getCurrentTime)
import Formail.GenKey
import System.Random (newStdGen)
import Formail.Types
import Formail.Messages

data ContactRequest = ContactRequest {
  apiKey :: Text,
  contactName :: Text,
  contactPhone :: Text,
  contactEmail :: Text,
  subject :: Text,
  message :: Text
  } deriving (Show, Eq)


handleContactRequest :: FormailConfiguration -> ContactRequest -> ServerPartT IO Text
handleContactRequest (FormailConfiguration _ dbConfig keysConfig _ smConfig _) (ContactRequest akey cname cphone cemail subj bod) = do
  dbconn <- liftIO $ connection dbConfig

  apiApp <- liftIO $ getValidAppFromKey dbconn akey

  case apiApp of
    Just (Saved mapp@(Application _ _ _ redUrl) _) ->do

      now <- liftIO getCurrentTime
      mid <- liftIO $ newHash keysConfig <$> newStdGen

      ((Saved msg _), new) <- liftIO $ insertMessage dbconn $ Message akey mid now cname cphone cemail subj bod

      if new then do
        liftIO $ sendMessage smConfig $ composeMessage mapp msg
        seeOther redUrl "Thank you for your request!"
        else seeOther redUrl "This was a duplicate!"

    Nothing -> notFound "No such application key."

getContactRequest :: RqData ContactRequest
getContactRequest =
  ContactRequest <$> lookText' "apikey" <*> lookText' "name" <*> lookText' "phone" <*> lookText' "subject" <*> lookText' "email" <*> lookText' "body"

handler :: FormailConfiguration -> ServerPart Text
handler config = do
  crr <- getDataFn getContactRequest

  case crr of
    (Left e) -> badRequest $ pack $ unlines e
    (Right cr) -> handleContactRequest config cr

apiV1Handler :: FormailConfiguration -> ServerPartT IO Text
apiV1Handler config = do
 decodeBody (defaultBodyPolicy "/tmp/" 0 10000 10000)
 dir "formail" $ handler config
