{-# Language OverloadedStrings, NoMonomorphismRestriction #-}

module Formail.Configuration
       (Config,
        DataFilesConfig(..),
        DatabaseConfig(..),
        KeysConfig(..),
        ServerConfig(..),
        SendmailConfig(..),
        LoggingConfig(..),
        FormailConfiguration(..),
        loadConfig)
  where

import Prelude hiding (lookup)
import Data.Configurator
import Data.Configurator.Types (Config)
import Data.Text (Text)
import GHC.Conc.Sync (ThreadId)
import System.FilePath
import System.Log (Priority(..))
import Paths_formail

import Formail.Options (ConfigurationOption(..))

data DataFilesConfig = DataFilesConfig {
  dataFilesRoot :: FilePath,
  migrationsRoot:: FilePath
  } deriving (Show, Eq)

data DatabaseConfig = DatabaseConfig {
  dbHost :: Maybe Text,
  dbPort :: Maybe Text,
  dbUsername :: Maybe Text,
  dbPassword :: Maybe Text,
  dbName :: Maybe Text
  } deriving (Show, Eq)

databaseConfig :: Config -> IO DatabaseConfig
databaseConfig config = DatabaseConfig <$>
  lookup config "database.host" <*>
  lookup config "database.port" <*>
  lookup config "database.username" <*>
  lookup config "database.password" <*>
  lookup config "database.dbname"

dataFilesConfig :: Config -> IO DataFilesConfig
dataFilesConfig config = do
  dd <- getDataDir
  mroot <- lookup config "data.root"
  mmig <- lookup config "data.migrations"

  return $ DataFilesConfig (maybe dd id mroot) (maybe (dd </> "migrations") id mmig)

data KeysConfig = KeysConfig {
  lifetimeDays :: Integer,
  entropyBits :: Int
  } deriving (Show, Eq)

keysConfig :: Config -> IO KeysConfig
keysConfig config = KeysConfig <$>
  require config "keys.lifetime_days" <*>
  require config "keys.entropy_bits"

data ServerConfig = ServerConfig {
  httpPort :: Int
  } deriving (Show, Eq)

data SendmailConfig = SendmailConfig {
  sendmailPath :: FilePath,
  sendmailFlags :: [String]
  } deriving (Show, Eq)

sendmailConfig :: Config -> IO SendmailConfig
sendmailConfig config = SendmailConfig <$>
  require config "sendmail.path" <*>
  lookupDefault [] config "sendmail.flags"

serverConfig :: Config -> IO ServerConfig
serverConfig config = ServerConfig <$> require config "http.port"

data LoggingConfig = LoggingConfig {
  accessLogPath :: FilePath,
  accessLogLevel :: Priority,
  opLogPath :: FilePath,
  opLogLevel :: Priority
  } deriving (Show, Eq)

loggingConfig :: Config -> IO LoggingConfig
loggingConfig config = LoggingConfig <$>
  require config "logging.access" <*>
  (read <$> require config "logging.access-level") <*>
  require config "logging.operations" <*>
  (read <$> require config "logging.operations-level")

data FormailConfiguration = FormailConfiguration {
  dataFiles :: DataFilesConfig,
  database :: DatabaseConfig,
  keys :: KeysConfig,
  server :: ServerConfig,
  sendmail :: SendmailConfig,
  logging :: LoggingConfig
  } deriving (Eq, Show)

loadConfig :: ConfigurationOption -> IO (FormailConfiguration, ThreadId)
loadConfig options = do
  (cfg, thr) <- autoReload autoConfig [Required $ configurationFile options]
  fcfg <- FormailConfiguration <$>
    dataFilesConfig cfg <*>
    databaseConfig cfg <*>
    keysConfig cfg <*>
    serverConfig cfg <*>
    sendmailConfig cfg <*>
    loggingConfig cfg

  return (fcfg, thr)
