{-# Language OverloadedStrings #-}

module Formail.Types.Client (
    Client(..)
    ) where

import Data.Text (Text)
import Formail.Types.Class (HasClientName(..))
import Database.PostgreSQL.Simple.FromRow

data Client = Client {
  clientClientName :: Text,
  clientEmail ::Text,
  clientContact :: Text
  }
  deriving (Eq, Show)

instance HasClientName Client where
  clientName = clientClientName

instance FromRow Client where
  fromRow = Client <$> field <*> field <*> field
