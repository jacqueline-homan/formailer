{-# Language OverloadedStrings #-}

module Formail.Types.Application (
  Application(..)
  ) where

import Formail.Types.Class
import Data.Text (Text)
import Database.PostgreSQL.Simple.FromRow

data Application = Application {
  applicationClientName :: Text,
  _applicationName :: Text,
  senderEmail :: Text,
  redirectToUrl :: Text
  } deriving (Eq, Show)

instance HasClientName Application where
  clientName = applicationClientName

instance HasApplicationName Application where
  applicationName = _applicationName

instance FromRow Application where
  fromRow = Application <$> field <*> field <*> field <*> field
