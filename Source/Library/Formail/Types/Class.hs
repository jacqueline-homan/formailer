module Formail.Types.Class (CreatedAt(..), HasClientName(..), HasApplicationName(..)) where

import Data.Time.Clock (UTCTime)
import Data.Text (Text)

class CreatedAt a where
  createdAt :: a -> Maybe UTCTime

class HasClientName a where
  clientName :: a -> Text

class HasApplicationName a where
  applicationName :: a -> Text
