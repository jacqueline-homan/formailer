{-# Language OverloadedStrings #-}

module Formail.Types.Message (
  Message(..), getMessageId
  ) where

import Data.Text (Text, unpack, intercalate, pack)
import Data.Time.Clock (UTCTime)
import Data.Time.Format (formatTime, defaultTimeLocale)
import Database.PostgreSQL.Simple.FromRow (FromRow(..), field)

data Message = Message {
  messageAppKey :: Text,
  messageId :: Text,
  messageDate :: UTCTime,
  contactName :: Text,
  contactPhone :: Text,
  contactEmail :: Text,
  subject :: Text,
  body :: Text
  } deriving (Eq)

instance Show Message where
  show (Message akey mid mdate cname cphone cemail subj _) = unpack $
    intercalate " " ["Message", akey, mid, pack (formatTime defaultTimeLocale "%F:%T" mdate), cname, cphone, cemail, subj]

instance FromRow Message where
  fromRow = Message <$> field <*> field <*> field <*> field <*> field <*> field <*> field <*> field

getMessageId :: Message -> Text
getMessageId (Message akey mid _ _ _ _ _ _) = intercalate "@" [mid, akey]
