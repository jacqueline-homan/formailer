{-# Language OverloadedStrings #-}

module Formail.Types.Key (Key(..), newKey)
  where

import Formail.Types.Class
import Data.Text (Text, unpack, intercalate, pack)
import Data.Time.Clock (UTCTime)
import Data.Time.Format (formatTime, defaultTimeLocale)
import Database.PostgreSQL.Simple.FromRow (FromRow(..), field)

data Key = Key {
  keyRipemd160 :: Text,
  keyClientName :: Text,
  keyApplicationName :: Text,
  keyCreatedAt :: Maybe UTCTime,
  keyExpiresAt :: UTCTime
  } deriving (Eq)

instance CreatedAt Key where
  createdAt = keyCreatedAt

instance HasClientName Key where
  clientName = keyClientName

instance HasApplicationName Key where
  applicationName = keyApplicationName

instance Show Key where
  show (Key hash kclient kapp _ expires) = unpack $ intercalate " " [
    "Key", hash, kclient, kapp, pack $ formatTime defaultTimeLocale "%F:%T" expires
    ]

instance FromRow Key where
  fromRow = Key <$> field <*> field <*> field <*> (Just <$> field) <*> field

newKey :: Text -> Text -> Text -> UTCTime -> Key
newKey ripemd cname aname expires = Key ripemd cname aname Nothing expires
