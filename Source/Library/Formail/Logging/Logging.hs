module Formail.Logging.Logging where


import System.Log.Logger (
  Priority(..),
  traplogging
  )
import Formail.Configuration (LoggingConfig(..))
import Formail.Logging.Internal

import Control.Exception (finally)

runWithLogging :: LoggingConfig -> IO a -> IO a
runWithLogging conf f = do
  lconf <- configureLogging conf
  (oh, lrp) <- startLogRotateThread conf lconf
  finally (traplogging opsLoggerName EMERGENCY "Unhandled Exception" f) (closeLogging oh lrp)
