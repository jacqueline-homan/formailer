module Formail.Logging.Internal where

import Control.Concurrent.Async
import Control.Concurrent.MVar
import System.IO
import System.Directory
import System.Posix.Signals
import System.Log.Logger (
  updateGlobalLogger,
  setLevel,
  Priority(..),
  Logger,
  setHandlers,
  removeAllHandlers
  )
import System.Log.Handler.Simple (streamHandler)
import Formail.Configuration (LoggingConfig(..))
import Formail.Logging.Types
import System.Posix.Files

opsLoggerName :: String
opsLoggerName = "Formail.Operations"

accessLoggerName :: String
accessLoggerName = "Happstack.Server.AccessLog.Combined"

openLogFile :: String -> FilePath -> Priority -> IO LogFile
openLogFile n pth prio = do
  h <- openFile pth AppendMode
  hr <- streamHandler h prio
  fs <- getFileStatus pth
  let lf = LogFile n pth prio h (deviceID fs) (fileID fs) hr
  updateGlobalLogger n $ configureLogger lf
  return lf

reopenLogFile :: LogFile -> IO LogFile
reopenLogFile lf@(LogFile nm pth prio hnd did fid _) = do
  isSame <- same

  if isSame then return lf else
    openLogFile nm pth prio
    >>= \l -> (hClose hnd >> return l)

  where
    same = do
      exists <- doesFileExist pth

      if not exists then return False else
        getFileStatus pth
        >>= \fs -> return $ (deviceID fs, fileID fs) == (did, fid)

configureLogger :: LogFile -> Logger -> Logger
configureLogger (LogFile _ _ prio _ _ _ handler) = setLevel prio . setHandlers [handler]

-- | Configures loggers for happstack and for general operational inforation.
configureLogging :: LoggingConfig -> IO Logging
configureLogging (LoggingConfig access apriority ops opriority) = do
  removeAllHandlers
    >> Logging
    <$> openLogFile accessLoggerName access apriority
    <*> openLogFile opsLoggerName ops opriority

closeLogging :: Handler -> LogRotateProc -> IO ()
closeLogging oh pr@(LogRotateProc _ _ lgv _ ) = do
  removeAllHandlers
  terminateLogRotate oh pr

  (Logging alog olog) <- takeMVar lgv
  hClose $ handle alog
  hClose $ handle olog

logRotateThread :: MVar Logging -> MVar RotateCommand -> IO ()
logRotateThread loggingv rc = do
  cmd <- takeMVar rc
  case cmd of
    Terminate -> return ()
    Rotate -> do
      (Logging oalf oolf) <- takeMVar loggingv

      Logging <$> reopenLogFile oalf <*> reopenLogFile oolf >>= putMVar loggingv

      logRotateThread loggingv rc

rotateLogs :: LogRotateProc -> IO ()
rotateLogs (LogRotateProc _ _ _ rc ) =
  tryPutMVar rc Rotate >> return ()

terminateLogRotate :: Handler -> LogRotateProc -> IO ()
terminateLogRotate oh (LogRotateProc thr _ _ rc) =
  putMVar rc Terminate >> wait thr >> installHandler sigHUP oh Nothing >> return ()

startLogRotateThread :: LoggingConfig -> Logging -> IO (Handler, LogRotateProc)
startLogRotateThread conf lg = do
  lconfv <- newMVar lg
  lrcv <- newEmptyMVar
  lasync <- async $ logRotateThread lconfv lrcv
  link lasync

  let lrp = LogRotateProc lasync conf lconfv lrcv

  oh <- installHandler sigHUP (Catch $ handleHup lrp) Nothing

  return (oh, lrp)

handleHup :: LogRotateProc -> IO ()
handleHup p = rotateLogs p
