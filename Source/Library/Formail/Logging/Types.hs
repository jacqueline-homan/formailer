module Formail.Logging.Types where


import System.IO
import System.Log.Logger (
  Priority(..),
  )
import System.Log.Handler.Simple (GenericHandler)
import Control.Concurrent.Async
import Formail.Configuration (LoggingConfig(..))
import Control.Concurrent.MVar
import System.Posix.Types

data LogFile = LogFile {
  name :: String,
  path :: FilePath,
  priority :: Priority,
  handle :: Handle,
  deviceId :: DeviceID,
  fileId :: FileID,
  logHandler :: GenericHandler Handle
  }

data Logging = Logging {
  accessLogFile :: LogFile,
  opLogFile :: LogFile
  }

data LogRotateProc = LogRotateProc {
  lrthread :: Async (),
  lconfig :: LoggingConfig,
  logging :: MVar Logging,
  commands :: MVar RotateCommand
  }

-- Log Rotation
--
-- Done in a separate thread so signal handler doesn't do IO
-- Thread waits on MVar for RotateCommand
-- If RotateCommand is Rotate, will reopen log files, reconfigure logging.
-- If RotateCommand is Terminate, will stop running thread.

data RotateCommand = Rotate | Terminate deriving (Eq, Show)
