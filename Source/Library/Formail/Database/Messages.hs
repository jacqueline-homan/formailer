{-# Language OverloadedStrings, QuasiQuotes, FlexibleContexts #-}

module Formail.Database.Messages where

import Formail.Database.Queries
import Formail.Types
import Database.PostgreSQL.Simple
import Database.PostgreSQL.Simple.SqlQQ
import Data.Text (Text)

getMessageByIdQuery :: Query
getMessageByIdQuery = [sql|
SELECT * FROM messages.message
  WHERE message_id = ?
  |]

getMessageById ::
  Connection -> Text -> IO (Maybe (Saved Message))
getMessageById conn mid = maybeOne conn getMessageByIdQuery [mid]

insertMessageQuery :: Query
insertMessageQuery = [sql|
INSERT INTO messages.message
  (key_ripemd160, message_id, message_date, contact_name, contact_phone, contact_email, subject, body)
VALUES
  (?, ?, ?, ?, ?, ?, ?, ?)
RETURNING *
  |]

insertMessage ::
  Connection -> Message -> IO ((Saved Message), Bool)
insertMessage conn (Message akey mid mdate cname cphone cemail subj bod) = do
  mm <- getMessageById conn mid

  case mm of
    Just m -> return (m, False)
    Nothing -> do
      n <- one conn insertMessageQuery (akey, mid, mdate, cname, cphone, cemail, subj, bod)
      return (n, True)
