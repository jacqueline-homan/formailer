{-# Language OverloadedStrings #-}
-- Create a database connection

module Formail.Database.Connection (connection) where

import Prelude hiding (concat)
import Formail.Configuration
import Database.PostgreSQL.Simple (Connection, connectPostgreSQL)
import Data.Text (Text, intercalate, replace, concat)
import Data.ByteString (ByteString)
import Data.Text.Encoding (encodeUtf8)

dbConnectionString :: DatabaseConfig -> ByteString
dbConnectionString dbconfig =
  encodeUtf8 $ intercalate " " $ map ckey $ zip fs ks
  where
    ckey :: ((DatabaseConfig -> Maybe Text), Text) -> Text
    ckey (c, k) =
      maybe "" (key k) (c dbconfig)

    key :: Text -> Text -> Text
    key k v = concat [k, "='", replace "'" "\\'" v, "'"]

    fs = [dbHost, dbPort, dbUsername, dbPassword, dbName]
    ks =   ["host", "port", "dbname", "password", "dbname"]

connection :: DatabaseConfig -> IO Connection
connection = connectPostgreSQL . dbConnectionString
