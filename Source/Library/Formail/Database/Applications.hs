{-# Language OverloadedStrings #-}

module Formail.Database.Applications where

import Formail.Database.Queries
import Database.PostgreSQL.Simple
import Data.Text (Text)

import Formail.Types

insertApplicationQuery :: Query
insertApplicationQuery =
  "INSERT INTO application.application (client_name, application_name, sender_email, redirect_to_url) VALUES (?, ?, ?, ?) RETURNING *"

insertApplication :: Connection -> Application -> IO ((Saved Application), Bool)
insertApplication conn application = do
  c_ <- getApplication conn $ applicationName application

  case c_ of
    Nothing -> do
      a <- one conn insertApplicationQuery (clientName application,
                                            applicationName application,
                                            senderEmail application,
                                            redirectToUrl application)
      return (a, True)
    (Just c) -> return (c, False)

getApplicationQuery :: Query
getApplicationQuery = "SELECT * FROM application.application WHERE application_name = ?"

getApplication :: Connection -> Text -> IO (Maybe (Saved Application))
getApplication conn name = maybeOne conn getApplicationQuery [name]
