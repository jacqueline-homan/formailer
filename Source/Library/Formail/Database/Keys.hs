{-# Language OverloadedStrings, QuasiQuotes #-}

module Formail.Database.Keys where

import Formail.Types
import Formail.Database.Queries
import Database.PostgreSQL.Simple
import Data.Text (Text)
import Database.PostgreSQL.Simple.SqlQQ

getKeyQuery :: Query
getKeyQuery = "SELECT * FROM application.appkey WHERE key_ripemd = ?"

getKey :: Connection -> Text -> IO (Maybe Key)
getKey conn hash = maybeOne conn getKeyQuery [hash]

getValidKeyQuery :: Query
getValidKeyQuery = "SELECT * FROM application.appkey WHERE key_ripemd160 = ? AND now() < expires_at"

getValidKey :: Connection -> Text -> IO (Maybe (Saved Key))
getValidKey conn hash = maybeOne conn getValidKeyQuery [hash]

getValidAppFromKeyQuery :: Query
getValidAppFromKeyQuery = [sql|
SELECT app.*
FROM application.application AS app
INNER JOIN application.appkey AS akey
ON
        app.client_name = akey.client_name and
        app.application_name = akey.application_name
WHERE akey.key_ripemd160 = ? AND now() < expires_at |]

getValidAppFromKey :: Connection -> Text -> IO (Maybe (Saved Application))
getValidAppFromKey conn key = maybeOne conn getValidAppFromKeyQuery [key]

insertKeyQuery :: Query
insertKeyQuery =
  "INSERT INTO application.appkey (key_ripemd160, client_name, application_name, expires_at) VALUES (?, ?, ?, ?) RETURNING *"

insertKey :: Connection -> Key -> IO Key
insertKey conn (Key hash cname aname _ expires) = one conn insertKeyQuery (hash, cname, aname, expires)
