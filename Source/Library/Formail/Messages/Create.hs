{-# Language OverloadedStrings, QuasiQuotes, FlexibleContexts, TypeFamilies #-}

-- Create a message body from a message and an application

module Formail.Messages.Create (composeMessage, sendMessage) where

import Prelude hiding (concat, unlines)
import Formail.Types
import Formail.Configuration
import Network.Mail.Mime
import Text.Blaze.Html5 as H
import Text.Blaze.Html5.Attributes as A
import Data.Text (Text, concat, unlines, pack)
import Text.Blaze.Html.Renderer.Text (renderHtml)
import Data.Text.Template
import Data.Time.Format (formatTime, defaultTimeLocale, rfc822DateFormat)
import qualified Data.Text.Lazy as Lazy

messageHtml :: Message -> Html
messageHtml (Message _ _ _ cname cphone cemail subj bod) =
  docTypeHtml $ do
  H.head $ do
    H.title $ text $ concat ["Contact request from ", cname]
  H.body $ do
    H.h1 "Contact Request"
    H.p $ text $ concat ["You've received a contact request from ", cname]
    H.h2 "The request is regarding"
    H.p $ text subj
    H.h2 $ text $ concat [cname, "may be reached at"]
    H.table $ do
      H.tr $ do
        H.td "Phone"
        H.td $ text cphone
      H.tr $ do
        H.td "E-mail"
        H.td $ H.a ! href (toValue $ concat ["mailto:", cemail]) $ text cemail
      H.h2 "The message is:"
      H.p $ text bod

messageTextTemplate :: Text
messageTextTemplate = unlines [
  "You've received a contact request from $cname",
  "The request is regarding $subject",
  "$cname may be reached at",
  "",
  "    Phone: $phone",
  "    Email: $email",
  "",
  "The mesage is regarding $subj",
  "",
  "The message is:",
  "",
  "$body"]

messageText :: Message -> Lazy.Text
messageText (Message _ _ _ cname cphone cemail subj bod) =
  let
    ckeys :: [(Text, Text)]
    ckeys = [
      ("cname", cname),
      ("subject", subj),
      ("phone", cphone),
      ("email", cemail),
      ("body", bod)]
    ctx :: Context
    ctx k= maybe "" Prelude.id (lookup k ckeys)
  in
    substitute messageTextTemplate ctx

composeMessage :: Application -> Message -> Mail
composeMessage (Application cname _ from _) message@(Message _ _ mdate tname _ _ subj _) =
  let
    sender = Address (Just tname) from
    recipient = Address (Just cname) from
    txtAlt = plainPart $ messageText message
    htmlAlt = htmlPart $ renderHtml $ messageHtml message
    midh = ("Message-Id", getMessageId message)
    dateh = ("Date", pack $ formatTime defaultTimeLocale rfc822DateFormat mdate)
    subjh = ("Subject", subj)
    hdrs = [midh, dateh, subjh]
  in
    Mail sender [recipient] [] [] hdrs [[txtAlt, htmlAlt]]

sendMessage :: SendmailConfig -> Mail -> IO ()
sendMessage (SendmailConfig path flags) msg = renderSendMailCustom path flags msg
