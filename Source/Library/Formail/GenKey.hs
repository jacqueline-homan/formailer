{-# Language OverloadedStrings #-}

module Formail.GenKey (newHash) where

import Formail.Configuration (KeysConfig(..))
import Prelude hiding (foldr)
import Crypto.Hash.RIPEMD160 (hash)
import System.Random (randoms, RandomGen)
import Numeric (showHex)
import Data.Text (Text)
import Data.ByteString (foldr)
import qualified Data.Text as T
import qualified Data.ByteString as B

newHash :: RandomGen r => KeysConfig -> r -> Text
newHash config = hexKey . genKey
  where
    hexKey = T.pack . foldr byteToHex ""
    genKey = hash . B.pack . take keyBytes . randoms

    byteToHex b s = if b < 16 then "0" ++ showHex b s else showHex b s
    keyBytes = (entropyBits config) `div` 8
