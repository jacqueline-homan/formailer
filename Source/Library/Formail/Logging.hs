-- Configure and manage logging
--
-- Opens files for logs, configures logger.
-- Monitors file paths for inode changes or path deletions.
-- On inode change or path deletion:
--     Opens new files
--     Updates logging handlers with new handles
--     Closes old handles

module Formail.Logging (
  module Formail.Logging.Logging
) where

import Formail.Logging.Logging
