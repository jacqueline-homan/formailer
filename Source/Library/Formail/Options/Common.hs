module Formail.Options.Common (withInfo, textOption) where

import Options.Applicative
import Data.Text (Text, pack)

withInfo :: Parser a -> String -> ParserInfo a
withInfo opts desc = info (helper <*> opts) $ progDesc desc

parseText :: Monad m => String -> m Text
parseText s = return $ pack s

textOption :: Mod OptionFields Text -> Parser Text
textOption = option (str >>= parseText)
