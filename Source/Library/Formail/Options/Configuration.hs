-- Options for the configuration file.

module Formail.Options.Configuration (ConfigurationOption(..), configurationOption) where

import Options.Applicative

configurationOption :: Parser ConfigurationOption
configurationOption = ConfigurationOption <$>
  strOption (long "configuration" <>
             metavar "FILE" <>
             help "Read configuration from file FILE")

data ConfigurationOption = ConfigurationOption {
  configurationFile :: String
  } deriving (Show, Eq)
