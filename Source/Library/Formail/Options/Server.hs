-- Options for parsing server command options

module Formail.Options.Server (serverOptions, ServerOptions(..)) where

import Options.Applicative
import Formail.Options.Configuration

data ServerOptions = ServerOptions ConfigurationOption
  deriving (Show)

serverOptions :: Parser ServerOptions
serverOptions = ServerOptions <$> configurationOption
