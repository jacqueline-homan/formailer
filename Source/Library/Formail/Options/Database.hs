-- Options for parsing database command options

module Formail.Options.Database (DbCommand(..), DbOptions(..), dbOptions) where

import Options.Applicative
import Formail.Options.Common
import Formail.Options.Configuration
import Formail.Options.Clients
import Formail.Options.Applications
import Formail.Options.Keys

data DbCommand =
  List |
  Initialize|
  Migrate |
  Clients ClientCommand |
  Applications ApplicationCommand |
  Keys KeyCommand
  deriving (Show)

data DbOptions = DbOptions ConfigurationOption DbCommand
  deriving (Show)

dbCommand :: Parser DbCommand
dbCommand = subparser (
  command "list" ((pure List) `withInfo` "List migrations available.") <>
  command "initialize" ((pure Initialize) `withInfo` "Initialize the migrations system.") <>
  command "migrate" ((pure Migrate) `withInfo` "Load and apply all migrations.") <>
  command "clients" (Clients <$> clientCommand `withInfo` "Client commands") <>
  command "apps" (Applications <$> applicationCommand `withInfo` "Application commands") <>
  command "keys" (Keys <$> keyCommand `withInfo` "Key commands")
  )

dbOptions :: Parser DbOptions
dbOptions = DbOptions <$> configurationOption <*> dbCommand
