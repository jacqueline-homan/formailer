-- Options for Application commands.

module Formail.Options.Applications (
  ApplicationCommand(..),
  NewApplication(NewApplication),
  senderEmail,
  redirectTo,
  applicationCommand,
  newApplicationOptions,
  applicationNameOption,
  applicationSenderOption,
  applicationRedirectToOption
  ) where

import Data.Text (Text)
import Options.Applicative
import Formail.Options.Common
import Formail.Options.Clients (clientNameOption)
import Formail.Types.Class (HasClientName(..), HasApplicationName(..))

data ApplicationCommand =
  NewApplicationCommand NewApplication
  deriving (Show, Eq)

data NewApplication = NewApplication {
  applicationClientName :: Text,
  _applicationName :: Text,
  senderEmail :: Text,
  redirectTo :: Text
  } deriving (Show, Eq)

instance HasClientName NewApplication where
  clientName = applicationClientName

instance HasApplicationName NewApplication where
  applicationName = _applicationName

applicationCommand :: Parser ApplicationCommand
applicationCommand = subparser (
  command "new" (NewApplicationCommand <$> newApplicationOptions `withInfo` "Create a new application.")
  )

newApplicationOptions :: Parser NewApplication
newApplicationOptions = NewApplication <$>
  clientNameOption <*>
  applicationNameOption <*>
  applicationSenderOption <*>
  applicationRedirectToOption

applicationNameOption :: Parser Text
applicationNameOption =
  textOption (long "application-name" <>
          metavar "APP" <>
          help "Name for this application.")

applicationSenderOption :: Parser Text
applicationSenderOption =
  textOption (long "application-sender" <>
          metavar "SENDER" <>
          help "Sender e-mail for this application.")

applicationRedirectToOption :: Parser Text
applicationRedirectToOption =
  textOption (long "application-redirect" <>
               metavar "REDIRECT" <>
               help "URL to redirect to after sending e-mail.")
