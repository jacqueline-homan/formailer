-- Manage the database schema from the migrations.
--
-- Arguments
--
-- Command arguments:
-- --create: create the database
-- --migrate: apply migrations that aren't yet applied
-- --list-migrations: list all migrations and their status

module Main where

import Formail.Configuration
import Formail.Options
import Formail.Database
import Formail.Types
import System.Random (newStdGen)
import Formail.GenKey
import Data.Time.Clock (getCurrentTime, addUTCTime)

main :: IO ()
main = execParser (dbOptions `withInfo` "Manage the database for formail.") >>= run

run :: DbOptions -> IO ()
run (DbOptions confOpt cmd) = do
  (conf, _) <- loadConfig confOpt

  case cmd of
    List -> runList conf
    Initialize -> runInitialize conf
    Migrate -> runMigrations conf
    (Clients ccmd) -> runClientCommand conf ccmd
    Applications (acmd) -> runApplicationCommand conf acmd
    Keys (kcmd) -> runKeyCommand conf kcmd

runList :: FormailConfiguration -> IO ()
runList config =
  migrationFiles (dataFiles config) >>= mapM_ print

runInitialize :: FormailConfiguration -> IO ()
runInitialize config =
  connection (database config) >>=
  initMigrations (dataFiles config)

runMigrations :: FormailConfiguration -> IO ()
runMigrations config = do
  conn <- connection $ database config
  loadMigrations (dataFiles config) conn
  applyMigrations conn

runClientCommand :: FormailConfiguration -> ClientCommand -> IO ()
runClientCommand config cmd = do
  conn <- connection $ database config

  case cmd of
    NewClientCommand ncl  ->
      insertClient conn ncl >>= \(c, n) ->
      if n then print $ "New client created: " ++ show c
                else print "Client exists already."
    EditClientCommand ec@(EditClient cname _ _) -> do
      dbcl <- getClient conn cname
      case dbcl of
        Nothing -> print "No such client"
        Just (Saved oc _) ->
          updateClient conn (editedClient ec oc) >>= print

runApplicationCommand :: FormailConfiguration -> ApplicationCommand -> IO ()
runApplicationCommand config cmd = do
  conn <- connection $ database config

  case cmd of
    (NewApplicationCommand (NewApplication cli name sender redirect)) ->
      insertApplication conn (Application cli name sender redirect) >>= print

runKeyCommand :: FormailConfiguration -> KeyCommand -> IO ()
runKeyCommand config cmd = do
  conn <- connection $ database config
  let keysConf = keys config

  case cmd of
    NewKeyCommand (NewKey clname apname) -> do
      now <- getCurrentTime
      hash <- newHash keysConf <$> newStdGen
      let expires = fromIntegral (3600 * 24 * (lifetimeDays keysConf)) `addUTCTime ` now

      let nk = newKey hash clname apname expires

      insertKey conn nk >>= print
