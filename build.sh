#!/bin/sh

# Builds in a jail for /usr/local installation, creates a staging
# under dist/formail-pkg, creates tar.gz in dist/formail.tar.gz

sandbox="${PWD}"/.cabal-sandbox

PATH="${sandbox}"/bin:${PATH}

prefix=/usr/local/formailer

stage_root="${PWD}"/dist/formailer-pkg
etc="${stage_root}/${prefix}/etc"
rc_d="${stage_root}/${prefix}/etc/rc.d"

cabal sandbox init &&
    cabal install happy haskell-src-exts &&
    cabal install --only-dependencies &&
    cabal clean &&
    cabal configure --prefix="${prefix}" &&
    cabal build &&
    cabal register --global --gen-script &&
    sed -i bak "s#${sandbox}#${prefix}#g" register.sh &&
    cabal copy --destdir="${stage_root}" &&
    install -d -o root -g wheel -m 0755 "${rc_d}" &&
    install -o root -g wheel -m 0755  port/formail "${rc_d}" &&
    install -o root -g wheel -m 0644 Configuration/formail.conf.sample "${etc}" &&
    install -o root -g wheel -m 0755 register.sh "${stage_root}/${prefix}/bin/register-formail.sh" &&
    (cd "${stage_root}" && tar cvzf ../formail.tar.gz .)
