-- Client information schema.

create or replace function migrations.apply_00001_client_schema()
returns bigint
as $$

begin

        create schema client;

        create table client.client (
               client_name varchar(200) primary key not null,
               client_email varchar(200) not null,
               client_contact_name varchar(200) not null,
               created_at timestamp with time zone not null default current_timestamp
               );

        create index client_email_idx on client.client (client_email);
        create index client_contact_name_idx on client.client (client_contact_name);
        create index client_created_at_idx on client.client (created_at);

return 0;
end;
$$ language plpgsql;

select migrations.add_migration(1, 'client_schema', 0, 'root');
