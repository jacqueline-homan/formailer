#!/bin/sh

db=$1
load_only=$2

if [ -z "$db" ]; then
    echo "Must specify the database as first argument."
    exit 1
fi

echo "Applying migrations to database $db"

for f in [0-9]*_*.sql; do
    echo "$f"
    psql --set ON_ERROR_STOP=on --set ON_ERROR_ROLLBACK=on --file $f $db || break;
done

if [ "$load_only" != 'load_only' ]; then
    echo 'Applying all migrations'

    psql --set ON_ERROR_STOP=on --set ON_ERROR_ROLLBACK=on $db <<EOF
begin;
select migrations.run_all_migrations();
commit;
EOF

    if [ -x "post_apply_all.sql" ]; then
        echo "Running post-apply script."
        psql --single-transaction --set ON_ERROR_STOP=on --set ON_ERROR_ROLLBACK=on --file post_apply_all.sql $db
    fi
else
    echo "Load Only specified"
fi
